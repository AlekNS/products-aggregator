bin ?= bin
name ?= prod-aggregator

gobin := ${GOPATH}/bin
target = $(bin)/$(name)
gosrc := $(shell find . -type f -name '*.go' -not -path './vendor/*')
lintfolders := cmd internal pkg
ifdef BUILD_GIT_HASH
ifeq "$(origin BUILD_GIT_HASH)" "environment"
  git_hash := ${BUILD_GIT_HASH}
endif
else
  git_hash := $(shell git log -n 1 --pretty=format:'%h')
endif
app_version := $(shell cat VERSION)
linkopts ?= -extldflags=-static -w -s

build: $(target)

$(target): $(gosrc)
	@echo 'Build $(target) $(app_version):$(git_hash)'
	@mkdir -p '$(bin)'
	@cd cmd && go build -o ../$(target) -tags netgo -trimpath -ldflags "$(linkopts) \
		-X gitlab.com/alekns/prod-aggregator/internal.GitHash=$(git_commit) \
		-X gitlab.com/alekns/prod-aggregator/internal.AppVersion=$(app_version)"

grpcgen:
	@protoc \
	  -I=. \
	  -I=${GOPATH}/src \
	  -I=${GOPATH}/src/github.com/gogo/protobuf \
	  --gogofast_out=internal/ext \
	  --go-grpc_out=internal/ext \
	  --go-grpc_opt=paths=import \
	  pkg/rpc/productsload/service.proto
	@protoc \
	  -I=. \
	  -I=${GOPATH}/src \
	  -I=${GOPATH}/src/github.com/gogo/protobuf \
	  --gogofast_out=internal/ext \
	  --go-grpc_out=internal/ext \
	  --go-grpc_opt=paths=import \
	  pkg/rpc/productsquery/service.proto

fmt:
	@echo "Formating all go code"
	@$(foreach dir,$(lintfolders),gofmt -s -w $(dir);)
	@echo "Done"

run:
	@echo 'Run $(name)'
	$(bin)/$(name)

test:
	@echo 'Test $(name)'
	@go test -cpu 2 -v -parallel 2 -cover -race ./internal/... ./pkg/...

clean:
	@echo 'clean'
	rm -f $(target)

.PHONY: fmt vendor run test clean grpcgen

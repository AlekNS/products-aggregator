package spec

import (
	"context"

	"gitlab.com/alekns/prod-aggregator/internal/products"
)

// ProductLogOutToChan .
func ProductLogOutToChan(out chan<- []products.ProductLog) products.ProductLogOutFn {
	return func(ctx context.Context, products []products.ProductLog) error {
		select {
		case out <- products:
		case <-ctx.Done():
			return ctx.Err()
		}

		return nil
	}
}

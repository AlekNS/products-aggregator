package spec

import (
	"context"
	"encoding/csv"
	"fmt"
	"io"
	"time"

	"gitlab.com/alekns/prod-aggregator/internal/products"
)

// TransformBytesOfCSVToProductLog .
func TransformBytesOfCSVToProductLog(batchSize int) products.TransformBytesToProductLogFn {
	return func(ctx context.Context, src io.Reader, out products.ProductLogOutFn) error {
		results := make([]products.ProductLog, batchSize)

		csvReader := csv.NewReader(src)
		csvReader.Comma = ';'

		i := 0
		isHeaderSkip := true
		for {
			row, err := csvReader.Read()
			if err == io.EOF {
				break
			}
			if err != nil {
				return err
			}

			if isHeaderSkip {
				isHeaderSkip = false
				continue
			}

			if len(row) != 2 {
				return fmt.Errorf("invalid csv format, only 2 columns valid")
			}

			if len(row) == 0 {
				continue
			}

			LastUpdate, err := time.Parse(time.RFC3339, row[1])
			if err != nil {
				return err
			}

			results[i].Name = row[0]
			results[i].UpdatedCount = 1
			results[i].LastUpdate = LastUpdate

			if i >= batchSize-1 {
				err = out(ctx, results[0:batchSize])
				if err != nil {
					return err
				}
				results = make([]products.ProductLog, batchSize)
				i = -1
			}

			i++
		}

		if i > 0 {
			err := out(ctx, results[0:i])
			if err != nil {
				return err
			}
		}

		return nil
	}
}

package spec

import (
	"context"
	"sort"

	"gitlab.com/alekns/prod-aggregator/internal/products"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

// TransformProductLogPreAggregate .
func TransformProductLogPreAggregate(loaderID primitive.ObjectID) products.TransformProductLogFn {
	return func(ctx context.Context, items []products.ProductLog) ([]products.ProductLog, error) {
		results := make([]products.ProductLog, len(items))

		sort.Slice(items, func(a, b int) bool {
			if items[a].Name < items[b].Name {
				return true
			}
			return false
		})

		var addAggItem = false
		i, j := 0, 0
		result := items[0]
		result.JobID = loaderID

		for ; i < len(items)-1; i++ {
			if items[i].Name != items[i+1].Name {
				addAggItem = true
			}
			if result.LastUpdate.Before(items[i].LastUpdate) {
				result.LastUpdate = items[i].LastUpdate
			}

			if addAggItem {
				results[j] = result
				j++
				result = items[i+1]
				result.JobID = loaderID
				result.UpdatedCount = 0

				addAggItem = false
			}
			result.UpdatedCount++
		}

		if !addAggItem {
			results[j] = result
			j++
		}

		return results[0:j], nil
	}
}

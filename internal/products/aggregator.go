package products

import "context"

type (
	// Aggregator .
	Aggregator interface {
		// Start .
		Start(ctx context.Context) error
		// Stop .
		Stop(ctx context.Context) error
		// Gather .
		Gather(ctx context.Context) error
	}
)

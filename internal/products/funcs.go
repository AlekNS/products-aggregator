package products

import (
	"context"
	"io"
)

type (
	// TransformProductLogFn .
	TransformProductLogFn func(ctx context.Context, products []ProductLog) ([]ProductLog, error)

	// ProductLogOutFn .
	ProductLogOutFn func(ctx context.Context, products []ProductLog) error

	// TransformBytesToProductLogFn .
	TransformBytesToProductLogFn func(ctx context.Context, src io.Reader, out ProductLogOutFn) error
)

package products

import (
	"context"

	"gitlab.com/alekns/prod-aggregator/internal/ext/rpc/productsqueryserver"
)

type (
	// Query .
	Query interface {
		// GetList .
		GetList(ctx context.Context, request *productsqueryserver.GetListRequest) (*productsqueryserver.GetListResponse, error)
	}
)

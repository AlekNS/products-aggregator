package products

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type (
	// ProductLog .
	ProductLog struct {
		// ID .
		ID []byte
		// JobID
		JobID primitive.ObjectID
		// Name .
		Name string
		// LastUpdate .
		LastUpdate time.Time
		// UpdatedCount .
		UpdatedCount uint32
	}
)

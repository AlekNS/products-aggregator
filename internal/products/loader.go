package products

import (
	"context"

	"gitlab.com/alekns/prod-aggregator/internal/ext/rpc/productsloadserver"
)

type (
	// Loader .
	Loader interface {
		// Fetch .
		Fetch(ctx context.Context, request *productsloadserver.FetchRequest) (*productsloadserver.FetchResponse, error)
	}
)

package singlemongo

import (
	"context"
	"errors"
	"fmt"
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/alekns/prod-aggregator/internal/ext/rpc/productsloadserver"
	"gitlab.com/alekns/prod-aggregator/internal/products"
	"gitlab.com/alekns/prod-aggregator/internal/products/spec"
	"gitlab.com/alekns/prod-aggregator/pkg/network"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type (
	mongoLoader struct {
		log *logrus.Entry
		db  *MongoDBService

		batchSize    int
		livenessTime int
		connTimeout  time.Duration
		fetchTimeout time.Duration
	}
)

const (
	// LoaderStatePending .
	LoaderStatePending = 0
	// LoaderStateProcessing .
	LoaderStateProcessing = iota
	// LoaderStateFinish .
	LoaderStateFinish
	// LoaderStateError .
	LoaderStateError
)

var _ = (products.Loader)((*mongoLoader)(nil))

// ErrFailedRefreshLivenessLoader .
var ErrFailedRefreshLivenessLoader = errors.New("Error")

// ErrFailedFinishLoader .
var ErrFailedFinishLoader = errors.New("Error")

func (ma *mongoLoader) Fetch(ctx context.Context, request *productsloadserver.FetchRequest) (*productsloadserver.FetchResponse, error) {
	// try to get http
	respHTTP, err := network.HTTPRequestAndGetResponse(ctx, ma.fetchTimeout, "GET", request.Url, nil, nil)
	if err != nil {
		return nil, err
	}

	// to know actual upload collection
	loaderCollectionName, err := getStateStr(ctx, ma.db.configCollection, loadCollectionStateStr)
	if err != nil {
		respHTTP.Body.Close()
		return nil, err
	}

	// start work and get id
	loaderID, err := ma.start(ctx, loaderCollectionName, ma.livenessTime)
	if err != nil {
		respHTTP.Body.Close()
		return nil, err
	}

	log := ma.log.WithField("jobId", fmt.Sprintf("%x", loaderID))

	upstreamStateCh := make(chan error, 1)
	itemsCh := make(chan []products.ProductLog, 1)
	transformCSVReader := spec.TransformBytesOfCSVToProductLog(ma.batchSize)
	outputToChannel := spec.ProductLogOutToChan(itemsCh)
	transformPreAggregation := spec.TransformProductLogPreAggregate(loaderID)

	go func() {
		defer respHTTP.Body.Close()

		// stop if loading is too long
		loadCtx, cancel := context.WithTimeout(ctx, ma.fetchTimeout)
		defer cancel()

		ma.log.Debugf("Fetch URL %s", request.Url)

		err := transformCSVReader(loadCtx, respHTTP.Body, outputToChannel)
		if err != nil {
			log.Errorf("Fail to fetch %s: %s", request.Url, err)

			upstreamStateCh <- err
			return
		}

		upstreamStateCh <- nil
	}()

	go func() {
		interval := time.Second * time.Duration(ma.livenessTime) / 3
		timer := time.NewTimer(interval)
		defer timer.Stop()
	stopLoader:
		for {
			select {
			case upstreamStateErr := <-upstreamStateCh:
				if upstreamStateErr == nil {
					break stopLoader
				} else {
					timer.Stop()
					return
				}
			case <-ctx.Done():
				return
			case <-timer.C:
				timer.Reset(interval)
				err := ma.livenessLoader(ctx, loaderID, ma.livenessTime)
				if err != nil {
					log.Errorf("fail to liveness job: %s", err)
				}
			case rawItems := <-itemsCh:
				aggRawItems, err := transformPreAggregation(ctx, rawItems)
				if err != nil {
					log.Errorf("fail to aggregate transform: %s", err)
					return
				}

				err = ma.insertBatch(ctx, loaderCollectionName, aggRawItems)
				if err != nil {
					log.Errorf("fail to insertBatch: %s", err)
					return
				}
			}
		}

		// save last item if exists
		select {
		case rawItems := <-itemsCh:
			aggRawItems, err := transformPreAggregation(ctx, rawItems)
			if err != nil {
				log.Errorf("fail to aggregate transform: %s", err)
				return
			}

			err = ma.insertBatch(ctx, loaderCollectionName, aggRawItems)
			if err != nil {
				log.Errorf("fail to insertBatch last: %s", err)
				return
			}
		default:
		}

		err := ma.finish(ctx, loaderID)
		if err != nil {
			// fail to finish
			// @TODO: mark load as invlid
		}
	}()

	return &productsloadserver.FetchResponse{
		FetchId: loaderID[:],
	}, nil
}

func (ma *mongoLoader) start(ctx context.Context, loadCollectionName string, expireTimeoutSec int) (primitive.ObjectID, error) {
	insertResult, err := ma.db.loaderCollection.InsertOne(ctx, bson.M{
		"expire":     time.Now().Add(time.Second * time.Duration(expireTimeoutSec)).Unix(),
		"state":      LoaderStateProcessing,
		"collection": loadCollectionName,
	})

	if err != nil {
		return primitive.ObjectID{}, err
	}

	return insertResult.InsertedID.(primitive.ObjectID), nil
}

func (ma *mongoLoader) livenessLoader(ctx context.Context, loaderID primitive.ObjectID, expireTimeoutSec int) error {
	updateResult, err := ma.db.loaderCollection.UpdateOne(ctx,
		bson.M{
			"_id": loaderID,
			"expire": bson.M{
				"$gt": time.Now().Unix(),
			},
			"state": LoaderStateProcessing,
		},
		bson.M{
			"$set": bson.M{
				"expire": time.Now().Add(time.Second * time.Duration(expireTimeoutSec)).Unix(),
			},
		},
	)
	if err != nil {
		return err
	}

	if updateResult.ModifiedCount != 1 {
		return ErrFailedRefreshLivenessLoader
	}

	return nil
}

func (ma *mongoLoader) finish(ctx context.Context, loaderID primitive.ObjectID) error {
	updateResult, err := ma.db.loaderCollection.UpdateOne(ctx,
		bson.M{
			"_id": loaderID,
			"expire": bson.M{
				"$gt": time.Now().Unix(),
			},
			"state": LoaderStateProcessing,
		},
		bson.M{
			"$set": bson.M{"state": LoaderStateFinish},
		},
	)

	if err != nil {
		return err
	}

	if updateResult.ModifiedCount != 1 {
		return ErrFailedFinishLoader
	}

	return nil
}

func (ma *mongoLoader) insertBatch(
	ctx context.Context,
	productCollectionName string,
	items []products.ProductLog,
) error {
	batch := make([]mongo.WriteModel, len(items))

	for i, item := range items {
		batch[i] = mongo.NewInsertOneModel().SetDocument(bson.M{
			"jobId":        item.JobID,
			"name":         item.Name,
			"lastUpdate":   item.LastUpdate,
			"updatedCount": item.UpdatedCount,
		})
	}

	_, err := ma.db.getProductsCollection(productCollectionName).BulkWrite(ctx, batch, &options.BulkWriteOptions{})
	if err != nil {
		return err
	}

	return nil
}

// NewMongoLoader .
func NewMongoLoader(log *logrus.Logger, db *MongoDBService) products.Loader {
	return &mongoLoader{
		db:           db,
		batchSize:    4096,
		livenessTime: 6,
		fetchTimeout: time.Minute,

		log: log.WithField("service", "mongoLoader"),
	}
}

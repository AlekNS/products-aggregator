package singlemongo

import (
	"context"

	"go.mongodb.org/mongo-driver/bson"
	mongodb "go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// StateValue .
type StateValue struct {
	Value interface{} `bson:"value"`
}

const (
	aggregationStateInt     = "aggregationState"
	loadCollectionStateStr  = "loadToCollection"
	queryCollectionStateStr = "queryFromCollection"
)

func getStateStr(ctx context.Context, collection *mongodb.Collection, name string) (string, error) {
	state := collection.FindOne(ctx, bson.M{"_id": name})

	if state.Err() != nil {
		return "", state.Err()
	}

	cfg := StateValue{}
	err := state.Decode(&cfg)
	if err != nil {
		return "", err
	}

	return cfg.Value.(string), nil
}

func setStateStr(ctx context.Context, collection *mongodb.Collection, name, value string) (string, error) {
	cfg := StateValue{Value: value}

	upsert := true
	returnDoc := options.Before

	state := collection.FindOneAndReplace(ctx,
		bson.M{"_id": name}, &cfg, &options.FindOneAndReplaceOptions{
			Upsert:         &upsert,
			ReturnDocument: &returnDoc,
		})

	if state.Err() != nil {
		return "", state.Err()
	}

	err := state.Decode(&cfg)
	if err != nil {
		return "", err
	}

	return cfg.Value.(string), nil
}

func getStateInt(ctx context.Context, collection *mongodb.Collection, name string) (int, error) {
	state := collection.FindOne(ctx, bson.M{"_id": name})

	if state.Err() != nil {
		return 0, state.Err()
	}

	cfg := StateValue{}
	err := state.Decode(&cfg)
	if err != nil {
		return 0, err
	}

	return int(cfg.Value.(int32)), nil
}

func setStateInt(ctx context.Context, collection *mongodb.Collection, name string, value int) (int, error) {
	cfg := StateValue{Value: value}

	upsert := true
	returnDoc := options.Before

	state := collection.FindOneAndReplace(ctx,
		bson.M{"_id": name}, &cfg, &options.FindOneAndReplaceOptions{
			Upsert:         &upsert,
			ReturnDocument: &returnDoc,
		})

	if state.Err() != nil {
		return 0, state.Err()
	}

	err := state.Decode(&cfg)
	if err != nil {
		return 0, err
	}

	return int(cfg.Value.(int32)), nil
}

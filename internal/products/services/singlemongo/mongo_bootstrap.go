package singlemongo

import (
	"context"
	"fmt"

	"github.com/sirupsen/logrus"
	"gitlab.com/alekns/prod-aggregator/internal/products"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

type (
	mongoBootstrap struct {
		db  *MongoDBService
		log *logrus.Entry
	}
)

func (ma *mongoBootstrap) Launch(ctx context.Context) error {
	const prefix = "failed to bootstrap: "
	var err error

	log := ma.log.WithField("method", "Launch")

	log.Debug("Bootstrap app")

	_, err = getStateInt(ctx, ma.db.configCollection, aggregationStateInt)
	if err == mongo.ErrNoDocuments {
		log.Info("First launch app, bootstrapping...")

		_, err = setStateStr(ctx, ma.db.configCollection, queryCollectionStateStr, "products1")
		if err != nil && err != mongo.ErrNoDocuments {
			return fmt.Errorf("%s%w", prefix, err)
		}

		_, err = setStateStr(ctx, ma.db.configCollection, loadCollectionStateStr, "products1")
		if err != nil && err != mongo.ErrNoDocuments {
			return fmt.Errorf("%s%w", prefix, err)
		}

		err = ma.createFetchCollection(ctx, "fetch")
		if err != nil {
			return fmt.Errorf("%s%w", prefix, err)
		}

		_, err = setStateInt(ctx, ma.db.configCollection, aggregationStateInt, mongoAggRefreshNextCollectionState)
		if err != nil && err != mongo.ErrNoDocuments {
			return fmt.Errorf("%s%w", prefix, err)
		}

		log.Info("Done")
	} else {
		log.Debug("Nothing to do")
	}

	return nil
}

func (ma *mongoBootstrap) createFetchCollection(ctx context.Context, collectionName string) error {
	ma.log.Debugf("Create collection: %s", collectionName)

	err := ma.db.db.CreateCollection(ctx, collectionName)
	if err != nil {
		return err
	}

	_, err = ma.db.db.Collection(collectionName).Indexes().CreateMany(ctx, []mongo.IndexModel{
		{
			Keys: bson.M{
				"expire": 1,
				"state":  1,
			},
		},
	})

	if err != nil {
		return err
	}

	return nil
}

// NewMongoBootstrap .
func NewMongoBootstrap(log *logrus.Logger, db *MongoDBService) products.Bootstrap {
	return &mongoBootstrap{
		db:  db,
		log: log.WithField("service", "mongoBootstrap"),
	}
}

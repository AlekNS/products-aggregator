package singlemongo

import (
	"context"
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/alekns/prod-aggregator/internal/products"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type (
	mongoAggregator struct {
		db  *MongoDBService
		log *logrus.Entry

		queryCollectionName string
		loadCollectionName  string

		recoveredState bool
		state          int
		interval       int

		stopCh    chan struct{}
		stoppedCh chan struct{}
	}
)

const (
	mongoAggRefreshNextCollectionState        = 0
	mongoAggSwitchLoaderToNextCollectionState = iota + 1
	mongoAggGatherState
	mongoAggSwitchQueryToNextCollectionState
)

func (ma *mongoAggregator) recoverState(ctx context.Context) error {
	readQueryState, err := getStateStr(ctx, ma.db.configCollection, queryCollectionStateStr)
	if err != nil && err != mongo.ErrNoDocuments {
		return err
	}
	ma.queryCollectionName = readQueryState

	readLoadState, err := getStateStr(ctx, ma.db.configCollection, loadCollectionStateStr)
	if err != nil && err != mongo.ErrNoDocuments {
		return err
	}
	ma.loadCollectionName = readLoadState

	readAggState, err := getStateInt(ctx, ma.db.configCollection, aggregationStateInt)
	if err != nil && err != mongo.ErrNoDocuments {
		return err
	}

	if readAggState != mongoAggRefreshNextCollectionState {
		ma.log.Warn("Aggregation state was recovered from the interrupted step")

		ma.recoveredState = true
		ma.state = readAggState
	} else {
		ma.state = mongoAggRefreshNextCollectionState
	}

	return nil
}

func (ma *mongoAggregator) setAggregationState(ctx context.Context, state int) error {
	ma.state = state

	_, err := setStateInt(ctx, ma.db.configCollection, aggregationStateInt, state)

	return err
}

func (ma *mongoAggregator) Start(ctx context.Context) error {
	ma.stopCh = make(chan struct{})
	ma.stoppedCh = make(chan struct{})

	ma.log.Info("Start aggregation")

	interval := time.Second * time.Duration(ma.interval)

	go func() {
		runFirstTime := make(chan struct{}, 1)
		runFirstTime <- struct{}{}
		timer := time.NewTimer(interval)
		defer timer.Stop()
		defer close(ma.stoppedCh)

		for {
			select {
			case <-ma.stopCh:
				close(ma.stoppedCh)
				return
			case <-runFirstTime:
			case <-timer.C:
			case <-ctx.Done():
				return
			}

			err := ma.Gather(ctx)
			if err != nil {
				ma.log.Errorf("fail to gather info: %s", err)
			}

			timer.Reset(interval)
		}
	}()

	return nil
}

func (ma *mongoAggregator) Stop(ctx context.Context) error {
	ma.log.Info("Stop aggregation")

	select {
	case <-ctx.Done():
		return ctx.Err()
	case <-ma.stoppedCh:
		return nil
	default:
	}

	close(ma.stopCh)
	<-ma.stoppedCh
	return nil
}

func (ma *mongoAggregator) recreateCollection(ctx context.Context, collectionName string) error {
	ma.log.Debugf("Recreate collection: %s", collectionName)

	err := ma.db.db.Collection(collectionName).Drop(ctx)
	if err != nil && err != mongo.ErrNilDocument {
		return err
	}

	err = ma.db.db.CreateCollection(ctx, collectionName, &options.CreateCollectionOptions{})
	if err != nil {
		return err
	}

	_, err = ma.db.db.Collection(collectionName).Indexes().CreateMany(ctx, []mongo.IndexModel{
		{
			Keys: bson.M{
				"updatedCount": 1,
				"_id":          1,
			},
			Options: &options.IndexOptions{
				PartialFilterExpression: bson.M{
					"aggregated": true,
				},
			},
		},
		{
			Keys: bson.M{
				"lastUpdate": 1,
				"_id":        1,
			},
			Options: &options.IndexOptions{
				PartialFilterExpression: bson.M{
					"aggregated": true,
				},
			},
		},
		{
			Keys: bson.M{
				"name": 1,
				"_id":  1,
			},
			Options: &options.IndexOptions{
				PartialFilterExpression: bson.M{
					"aggregated": true,
				},
			},
		},
	})

	if err != nil {
		return err
	}

	return nil
}

func (ma *mongoAggregator) waitLoadOperations(ctx context.Context) error {
	ma.log.Debugf("Wait fetching")

	interval := time.Second

	for {
		result, err := ma.db.loaderCollection.CountDocuments(ctx, bson.M{
			"expire": bson.M{
				"$gt": time.Now().Unix(),
			},
			"state":      LoaderStateProcessing,
			"collection": ma.queryCollectionName,
		})
		if err != nil {
			return err
		}

		if result == 0 {
			return nil
		}

		ma.log.Tracef("Fetches to %s in progress: %d", ma.queryCollectionName, result)

		timer := time.NewTimer(interval)
		select {
		case <-ctx.Done():
			timer.Stop()
			return ctx.Err()
		case <-timer.C:
			timer.Stop()
		}
	}
}

func (ma *mongoAggregator) beforeAggregate(ctx context.Context) error {
	nextLoadCollectionName := ma.db.getNextProductsCollection(ma.loadCollectionName)

	// drop next collection and create one
	if ma.state == mongoAggRefreshNextCollectionState {
		ma.log.Debugf("Prepare next collection")

		err := ma.recreateCollection(ctx, nextLoadCollectionName.Name())
		if err != nil {
			return err
		}

		err = ma.setAggregationState(ctx, mongoAggSwitchLoaderToNextCollectionState)
		if err != nil {
			return err
		}
	}

	// set uploads to next and wait to complete last uploads
	if ma.state == mongoAggSwitchLoaderToNextCollectionState {
		ma.log.Debugf("Switch loaders to next collection")

		if ma.queryCollectionName == ma.loadCollectionName {
			_, err := setStateStr(ctx, ma.db.configCollection, loadCollectionStateStr, nextLoadCollectionName.Name())
			if err != nil {
				return err
			}
			ma.loadCollectionName = nextLoadCollectionName.Name()
		}

		err := ma.waitLoadOperations(ctx)
		if err != nil {
			return err
		}

		err = ma.setAggregationState(ctx, mongoAggGatherState)
		if err != nil {
			return err
		}
	}

	return nil
}

func (ma *mongoAggregator) getInvalidJobIDs(ctx context.Context, productsCollectionName string) ([]primitive.ObjectID, error) {
	jobsResult, err := ma.db.loaderCollection.Find(ctx, bson.M{
		"expire": bson.M{
			"$lte": time.Now().Unix(),
		},
		"state": bson.M{
			"$in": bson.A{LoaderStateProcessing},
		},
	})
	if err != nil {
		return nil, err
	}

	invalidJobs := make([]primitive.ObjectID, 0, jobsResult.RemainingBatchLength())
	for jobsResult.Next(ctx) {
		invalidJobs = append(invalidJobs, jobsResult.Current.Index(0).Value().ObjectID())
	}

	return invalidJobs, nil
}

func (ma *mongoAggregator) aggregate(ctx context.Context) error {
	ma.log.Debugf("Run aggregation")

	nextQueryCollection := ma.db.getNextProductsCollection(ma.queryCollectionName)

	// find not completed loaders
	invalidJobs, err := ma.getInvalidJobIDs(ctx, ma.queryCollectionName)
	if err != nil {
		return err
	}

	// clean stable aggregation
	_, err = ma.db.getProductsCollection(ma.queryCollectionName).Aggregate(ctx, mongo.Pipeline{
		bson.D{{
			"$match", bson.M{
				"jobId": bson.M{
					"$nin": invalidJobs,
				},
			},
		}},
		bson.D{{
			"$group", bson.M{
				"_id":          "$name",
				"lastUpdate":   bson.M{"$max": "$lastUpdate"},
				"updatedCount": bson.M{"$sum": "$updatedCount"},
			},
		}},
		bson.D{{
			"$project", bson.M{
				"_id": bson.M{
					"$function": bson.M{
						"body": "function (id) { return hex_md5(id) }",
						"args": bson.A{"$_id"},
						"lang": "js",
					},
				},
				"name":         "$_id",
				"aggregated":   bson.M{"$literal": true},
				"lastUpdate":   "$lastUpdate",
				"updatedCount": "$updatedCount",
			},
		}},
		bson.D{{
			"$merge", bson.M{
				"into": nextQueryCollection.Name(),
			},
		}},
	})
	if err != nil {
		return err
	}

	return ma.setAggregationState(ctx, mongoAggSwitchQueryToNextCollectionState)
}

func (ma *mongoAggregator) afterAggregate(ctx context.Context) error {
	ma.log.Debugf("Switch query collection")

	if ma.queryCollectionName != ma.loadCollectionName {
		invalidJobs, err := ma.getInvalidJobIDs(ctx, ma.loadCollectionName)
		if err != nil {
			return err
		}

		if len(invalidJobs) > 0 {
			// mark invalid jobs with error
			_, err = ma.db.loaderCollection.UpdateMany(ctx, bson.M{
				"_id": bson.M{
					"$in": invalidJobs,
				},
			}, bson.M{
				"$set": bson.M{
					"state": LoaderStateError,
				},
			})
			if err != nil {
				return err
			}
		}

		nextQueryCollection := ma.db.getNextProductsCollection(ma.queryCollectionName)

		_, err = setStateStr(ctx, ma.db.configCollection, queryCollectionStateStr, nextQueryCollection.Name())
		if err != nil {
			return err
		}

		ma.queryCollectionName = nextQueryCollection.Name()
	}

	return ma.setAggregationState(ctx, mongoAggRefreshNextCollectionState)
}

func (ma *mongoAggregator) Gather(ctx context.Context) error {
	var err error

	err = ma.recoverState(ctx)
	if err != nil {
		return err
	}

	if ma.state == mongoAggRefreshNextCollectionState || ma.state == mongoAggSwitchLoaderToNextCollectionState {
		err = ma.beforeAggregate(ctx)
		if err != nil {
			return err
		}
	}

	if ma.state == mongoAggGatherState {
		err = ma.aggregate(ctx)
		if err != nil {
			return err
		}
	}

	if ma.state == mongoAggSwitchQueryToNextCollectionState {
		err = ma.afterAggregate(ctx)
		if err != nil {
			return err
		}
	}

	return nil
}

// NewMongoAggregator .
func NewMongoAggregator(log *logrus.Logger, db *MongoDBService, interval int) products.Aggregator {
	return &mongoAggregator{
		db:  db,
		log: log.WithField("service", "mongoAggregator"),

		interval: interval,
	}
}

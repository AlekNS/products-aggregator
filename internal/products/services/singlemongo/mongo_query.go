package singlemongo

import (
	"context"

	"github.com/sirupsen/logrus"
	"gitlab.com/alekns/prod-aggregator/internal/ext/rpc/productsqueryserver"
	"gitlab.com/alekns/prod-aggregator/internal/products"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type (
	mongoQuery struct {
		log *logrus.Entry
		db  *MongoDBService
	}
)

var _ = (products.Query)((*mongoQuery)(nil))

func (ma *mongoQuery) GetList(ctx context.Context, request *productsqueryserver.GetListRequest) (*productsqueryserver.GetListResponse, error) {
	// to known where actual state collection
	queryCollectionName, err := getStateStr(ctx, ma.db.configCollection, queryCollectionStateStr)
	if err != nil {
		return nil, err
	}

	filter := bson.M{
		"aggregated": true,
	}

	// sorting
	sortOrder := 1
	if request.SortDesc {
		sortOrder = -1
	}

	sortOptions := bson.M{}
	switch request.SortBy {
	case productsqueryserver.ListSortByProductName:
		sortOptions["name"] = sortOrder
	case productsqueryserver.ListSortByLastUpdate:
		sortOptions["lastUpdate"] = sortOrder
	case productsqueryserver.ListSortByUpdatedCount:
		sortOptions["updatedCount"] = sortOrder
	default:
		sortOptions["_id"] = sortOrder
	}

	// slow limiting implementation
	// @todo:
	//   use long live cursor with grpc streams
	//   use multiversions for queryCollections (like mvcc)
	//   bookmark, use several collections sorted by sort fields (need stabel id)
	fetchCount := int64(request.Params.FetchCount)
	if fetchCount == 0 {
		fetchCount = 100
	}
	if fetchCount > 1000 {
		fetchCount = 1000
	}
	fetchSkip := int64(request.Params.FetchSkip)
	if fetchCount < 0 {
		fetchCount = 0
	}

	queryCollection, err := ma.db.getProductsCollection(queryCollectionName).Find(ctx, filter, &options.FindOptions{
		Sort:  sortOptions,
		Limit: &fetchCount,
		Skip:  &fetchSkip,
	})

	// query
	productLogItems := make([]products.ProductLog, 0, fetchCount)

	err = queryCollection.All(ctx, &productLogItems)
	if err != nil {
		return nil, err
	}

	// transform results
	items := make([]productsqueryserver.ListItem, len(productLogItems))
	for i, productItem := range productLogItems {
		items[i].Id = productItem.ID
		items[i].ProductName = productItem.Name
		items[i].LastUpdate = productItem.LastUpdate
		items[i].UpdatedCount = uint64(productItem.UpdatedCount)
	}

	return &productsqueryserver.GetListResponse{
		Items: items,
	}, nil
}

// NewMongoQuery .
func NewMongoQuery(log *logrus.Logger, db *MongoDBService) products.Query {
	return &mongoQuery{
		db:  db,
		log: log.WithField("service", "mongoQuery"),
	}
}

package singlemongo

import (
	mongodb "go.mongodb.org/mongo-driver/mongo"
)

// MongoDBService .
type MongoDBService struct {
	db *mongodb.Database

	configCollection *mongodb.Collection
	loaderCollection *mongodb.Collection

	products1Collection *mongodb.Collection
	products2Collection *mongodb.Collection
}

const (
	productsCollection1 = "products1"
	productsCollection2 = "products2"
)

func (svc *MongoDBService) getProductsCollection(collectionName string) *mongodb.Collection {
	if collectionName == productsCollection1 {
		return svc.products1Collection
	}
	return svc.products2Collection
}

func (svc *MongoDBService) getNextProductsCollection(collectionName string) *mongodb.Collection {
	if collectionName == productsCollection1 {
		return svc.products2Collection
	}
	return svc.products1Collection
}

// NewMongoDBService .
func NewMongoDBService(db *mongodb.Database) *MongoDBService {
	return &MongoDBService{
		db: db,

		configCollection: db.Collection("config"),
		loaderCollection: db.Collection("fetch"),

		products1Collection: db.Collection("products1"),
		products2Collection: db.Collection("products2"),
	}
}

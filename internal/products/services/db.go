package services

import (
	"context"
	"fmt"

	"github.com/sirupsen/logrus"
	"gitlab.com/alekns/prod-aggregator/internal/config"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// NewMongoClient .
func NewMongoClient(cfg *config.Settings, log *logrus.Logger) (*mongo.Client, error) {
	log.Debugf("Connecting to MongoDB: %s", cfg.Mongo.URL.Host)

	dbClientOptions := options.Client().ApplyURI(cfg.Mongo.URL.String())

	dbClient, err := mongo.Connect(context.TODO(), dbClientOptions)
	if err != nil {
		return nil, fmt.Errorf("fail connecting to Mongo: %w", err)
	}

	err = dbClient.Ping(context.TODO(), nil)
	if err != nil {
		return nil, fmt.Errorf("fail to ping Mongo: %w", err)
	}

	log.Info("Connected to MongoDB success")

	return dbClient, nil
}

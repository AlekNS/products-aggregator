package products

type (
	// InstanceFactory .
	InstanceFactory interface {
		Aggregator() (Aggregator, error)
		Loader() (Loader, error)
		Query() (Query, error)
		Bootstrap() (Bootstrap, error)
	}
)

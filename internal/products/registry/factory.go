package registry

import (
	"context"
	"strings"

	"github.com/sirupsen/logrus"
	"gitlab.com/alekns/prod-aggregator/internal/config"
	"gitlab.com/alekns/prod-aggregator/internal/products"
	"gitlab.com/alekns/prod-aggregator/internal/products/services"
	"gitlab.com/alekns/prod-aggregator/internal/products/services/singlemongo"
)

type (
	singleInstanceFactory struct {
		cfg    *config.Settings
		logger *logrus.Logger

		singleMongoSvc *singlemongo.MongoDBService
	}
)

func (i *singleInstanceFactory) Aggregator() (products.Aggregator, error) {
	return singlemongo.NewMongoAggregator(i.logger, i.singleMongoSvc, i.cfg.Aggregator.SwitchInterval), nil
}

func (i *singleInstanceFactory) Loader() (products.Loader, error) {
	return singlemongo.NewMongoLoader(i.logger, i.singleMongoSvc), nil
}

func (i *singleInstanceFactory) Query() (products.Query, error) {
	return singlemongo.NewMongoQuery(i.logger, i.singleMongoSvc), nil
}

func (i *singleInstanceFactory) Bootstrap() (products.Bootstrap, error) {
	return singlemongo.NewMongoBootstrap(i.logger, i.singleMongoSvc), nil
}

// NewInstanceFactory .
func NewInstanceFactory(ctx context.Context, cfg *config.Settings, logger *logrus.Logger) (products.InstanceFactory, error) {
	mongoClient, err := services.NewMongoClient(cfg, logger)
	if err != nil {
		return nil, err
	}

	if cfg.Mongo.DBMode == config.MongoDBModeSingle {
		db := singlemongo.NewMongoDBService(mongoClient.Database(strings.TrimLeft(cfg.Mongo.URL.Path, "/")))

		return &singleInstanceFactory{
			singleMongoSvc: db,
			cfg:            cfg,
			logger:         logger,
		}, nil
	} else {
		panic("Not implemented yet")
	}
}

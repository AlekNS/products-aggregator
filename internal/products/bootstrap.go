package products

import (
	"context"
)

type (
	// Bootstrap .
	Bootstrap interface {
		// Launch .
		Launch(ctx context.Context) error
	}
)

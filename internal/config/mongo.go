package config

import (
	"net/url"

	"github.com/spf13/viper"
	"gitlab.com/alekns/prod-aggregator/pkg/utils"
)

type (
	// MongoDbSettings .
	MongoDbSettings struct {
		URL    *url.URL
		DBMode string
	}
)

const (
	// MongoDBModeSingle .
	MongoDBModeSingle = "single"
	// MongoDBModeShard .
	MongoDBModeShard = "shard"
)

func mongoSettingsValidateAndGet(v *viper.Viper) *MongoDbSettings {
	v.SetDefault("mongodb.mode", "single")

	if v.GetString("mongodb.url") == "" {
		panic("Mongo url is empty")
	}

	var conf = &MongoDbSettings{
		URL:    utils.MustURLParse(v.GetString("mongodb.url")),
		DBMode: v.GetString("mongodb.mode"),
	}

	if conf.DBMode != MongoDBModeShard && conf.DBMode != MongoDBModeSingle {
		panic("Invalid mongo db mode, single or shard supported only")
	}

	if conf.URL.Scheme != "mongodb" {
		panic("Invalid mongo url, the schema of url is not like mongodb://")
	}

	return conf
}

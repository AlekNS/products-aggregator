package config

import (
	"net/url"

	"github.com/spf13/viper"
	"gitlab.com/alekns/prod-aggregator/pkg/utils"
)

type (
	// GrpcSettings .
	GrpcSettings struct {
		Bind *url.URL

		TokenSecret string

		TLS         bool
		TLSAuth     bool
		TLSCaFile   string
		TLSCertFile string
		TLSKeyFile  string
	}
)

func grpcSettingsValidateAndGet(v *viper.Viper) *GrpcSettings {
	const bindKey = "grpc.bind"

	v.SetDefault(bindKey, "tcp://localhost:10000")

	var conf = &GrpcSettings{
		Bind: utils.MustURLParse(v.GetString(bindKey)),

		TokenSecret: v.GetString("grpc.tokensecret"),

		TLS:         v.GetBool("grpc.tls"),
		TLSAuth:     v.GetBool("grpc.tlsauth"),
		TLSCaFile:   v.GetString("grpc.tlscafile"),
		TLSCertFile: v.GetString("grpc.tlscertfile"),
		TLSKeyFile:  v.GetString("grpc.tlskeyfile"),
	}

	return conf
}

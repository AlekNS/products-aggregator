package config

import (
	"github.com/spf13/viper"
)

type (
	// AggregatorSettings .
	AggregatorSettings struct {
		SwitchInterval int
	}
)

func aggregatorSettingsValidateAndGet(v *viper.Viper) *AggregatorSettings {
	v.SetDefault("aggregator.interval", 5)

	var conf = &AggregatorSettings{
		SwitchInterval: v.GetInt("aggregator.interval"),
	}

	return conf
}

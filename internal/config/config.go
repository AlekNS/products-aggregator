package config

import (
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

type (
	// Settings .
	Settings struct {
		LoggingConsole *LoggingConsoleSettings

		Mongo *MongoDbSettings

		Loader *LoaderSettings

		Aggregator *AggregatorSettings

		Grpc *GrpcSettings
	}
)

// NewSettings .
func NewSettings(v *viper.Viper) *Settings {
	return &Settings{
		LoggingConsole: loggingConsoleSettingsValidateAndGet(v),

		Mongo: mongoSettingsValidateAndGet(v),

		Loader: loaderSettingsValidateAndGet(v),

		Aggregator: aggregatorSettingsValidateAndGet(v),

		Grpc: grpcSettingsValidateAndGet(v),
	}
}

// ConfigName .
const ConfigName = "config"

// ServiceName .
const ServiceName = "prod-aggregator"

// LoadSettings .
func LoadSettings(cmd *cobra.Command, v *viper.Viper) *Settings {
	var flagConfig = cmd.Flag("config-file")

	if len(flagConfig.Value.String()) == 0 {
		v.SetConfigName(ConfigName)
		v.AddConfigPath("/etc/" + ServiceName)
		v.AddConfigPath("$HOME/." + ServiceName)
		v.AddConfigPath(".")
	} else {
		v.SetConfigFile(flagConfig.Value.String())
	}

	var err = v.ReadInConfig()
	if err != nil {
		panic(err)
	}

	return NewSettings(v)
}

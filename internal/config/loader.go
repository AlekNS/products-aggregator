package config

import (
	"github.com/spf13/viper"
)

type (
	// LoaderSettings .
	LoaderSettings struct {
		RequestTimeout int
	}
)

func loaderSettingsValidateAndGet(v *viper.Viper) *LoaderSettings {
	var conf = &LoaderSettings{
		RequestTimeout: v.GetInt("loader.request.timeout"),
	}

	return conf
}

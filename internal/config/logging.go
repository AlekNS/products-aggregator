package config

import (
	"github.com/spf13/viper"
)

type (
	// LoggingConsoleSettings .
	LoggingConsoleSettings struct {
		Level string `validate:"required,oneof=trace debug info warning error"`
	}
)

func loggingConsoleSettingsValidateAndGet(v *viper.Viper) *LoggingConsoleSettings {
	var conf = &LoggingConsoleSettings{
		Level: v.GetString("logging.console.level"),
	}

	return conf
}

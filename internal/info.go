package internal

// AppVersion .
const AppVersion = "1.0.0"

// AppName .
const AppName = "Products Aggregator"

// GitHash .
var GitHash string

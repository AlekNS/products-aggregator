package cmds

import (
	"context"
	"fmt"
	"os"
	"os/signal"
	"syscall"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/alekns/prod-aggregator/internal/config"
	"gitlab.com/alekns/prod-aggregator/internal/ext/rpc/grpcserver"
	"gitlab.com/alekns/prod-aggregator/internal/logger"
)

// ServeCommand .
func ServeCommand() *cobra.Command {
	cmd := &cobra.Command{
		Use:     "serve",
		Aliases: []string{"s"},
		Short:   "Serve mode",
		Run: func(cmd *cobra.Command, args []string) {
			cfg := config.LoadSettings(cmd, viper.GetViper())
			logger.InitLogger(cfg)

			ctx, stop := context.WithCancel(context.Background())
			defer stop()

			err := grpcserver.RunGrpcServer(ctx, logger.GetRootLogger(), cfg)
			if err != nil {
				logger.GetRootLogger().Fatalf("failed to run grpc server: %s", err)
			}

			appSignals := make(chan os.Signal, 1)
			signal.Notify(appSignals,
				syscall.SIGINT,
				syscall.SIGTERM,
				syscall.SIGQUIT,
			)
			go func() {
				sig := <-appSignals
				fmt.Println("Stop process. Catch signal", sig)
				stop()
			}()

			fmt.Println("Serve... Press CTRL-C to stop")
			<-ctx.Done()
		},
	}

	return cmd
}

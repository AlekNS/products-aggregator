package cmds

import (
	"context"
	"fmt"
	"os"
	"os/signal"
	"syscall"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/alekns/prod-aggregator/internal/config"
	"gitlab.com/alekns/prod-aggregator/internal/logger"
	"gitlab.com/alekns/prod-aggregator/internal/products/registry"
)

// AggregateCommand .
func AggregateCommand() *cobra.Command {
	cmd := &cobra.Command{
		Use:     "aggregate",
		Aliases: []string{"a"},
		Short:   "Aggregation mode",
		Run: func(cmd *cobra.Command, args []string) {
			cfg := config.LoadSettings(cmd, viper.GetViper())
			logger.InitLogger(cfg)

			ctx, stop := context.WithCancel(context.Background())
			defer stop()

			factory, err := registry.NewInstanceFactory(ctx, cfg, logger.GetRootLogger())
			if err != nil {
				logger.GetRootLogger().Fatalf("failed to init factory: %s", err)
			}

			aggregator, _ := factory.Aggregator()

			appSignals := make(chan os.Signal, 1)
			signal.Notify(appSignals,
				syscall.SIGINT,
				syscall.SIGTERM,
				syscall.SIGQUIT,
			)
			go func() {
				sig := <-appSignals
				fmt.Println("Stop process. Catch signal", sig)
				stop()
			}()

			aggregator.Start(ctx)

			fmt.Println("Aggregation... Press CTRL-C to stop")
			<-ctx.Done()

			aggregator.Stop(ctx)
		},
	}

	return cmd
}

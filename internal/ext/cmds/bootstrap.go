package cmds

import (
	"context"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/alekns/prod-aggregator/internal/config"
	"gitlab.com/alekns/prod-aggregator/internal/logger"
	"gitlab.com/alekns/prod-aggregator/internal/products/registry"
)

// BootstrapCommand .
func BootstrapCommand() *cobra.Command {
	cmd := &cobra.Command{
		Use:     "bootstrap",
		Aliases: []string{"b"},
		Short:   "Bootstrap application",
		Run: func(cmd *cobra.Command, args []string) {
			cfg := config.LoadSettings(cmd, viper.GetViper())
			logger.InitLogger(cfg)
			log := logger.GetRootLogger()

			ctx, stop := context.WithCancel(context.Background())
			defer stop()

			factory, err := registry.NewInstanceFactory(ctx, cfg, log)
			if err != nil {
				logger.GetRootLogger().Fatalf("failed to init factory: %s", err)
			}

			bootstrap, _ := factory.Bootstrap()

			err = bootstrap.Launch(ctx)
			if err != nil {
				log.Fatalf("Fail to bootstrap application: %s", err)
			}
		},
	}

	return cmd
}

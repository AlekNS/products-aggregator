package grpcserver

import (
	"context"

	pbquery "gitlab.com/alekns/prod-aggregator/internal/ext/rpc/productsqueryserver"
	"gitlab.com/alekns/prod-aggregator/internal/products"
)

type queryServer struct {
	pbquery.QueryService

	rootCtx context.Context
	factory products.InstanceFactory
}

func (svc *queryServer) GetList(ctx context.Context, in *pbquery.GetListRequest) (out *pbquery.GetListResponse, err error) {
	query, _ := svc.factory.Query()

	out, err = query.GetList(ctx, in)

	return
}

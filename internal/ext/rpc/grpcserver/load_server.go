package grpcserver

import (
	"context"

	pbload "gitlab.com/alekns/prod-aggregator/internal/ext/rpc/productsloadserver"
	"gitlab.com/alekns/prod-aggregator/internal/products"
)

type loadServer struct {
	pbload.ProductsLoadService

	rootCtx context.Context
	factory products.InstanceFactory
}

func (svc *loadServer) Fetch(ctx context.Context, in *pbload.FetchRequest) (out *pbload.FetchResponse, err error) {
	loader, _ := svc.factory.Loader()

	out, err = loader.Fetch(svc.rootCtx, in)

	return
}

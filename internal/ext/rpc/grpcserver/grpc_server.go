package grpcserver

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"io/ioutil"
	"log"
	"net"

	"github.com/sirupsen/logrus"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"

	"gitlab.com/alekns/prod-aggregator/internal/config"
	pbload "gitlab.com/alekns/prod-aggregator/internal/ext/rpc/productsloadserver"
	pbquery "gitlab.com/alekns/prod-aggregator/internal/ext/rpc/productsqueryserver"
	"gitlab.com/alekns/prod-aggregator/internal/products/registry"
)

const defaultAuthHeaderName = "authorization"

func getTLSConfig(cfg *config.GrpcSettings) *tls.Config {
	caPemData, err := ioutil.ReadFile(cfg.TLSCaFile)
	if err != nil {
		log.Fatalf("failed to load ca for tls: %s", err)
	}

	tlsCfg := &tls.Config{}
	tlsCfg.RootCAs = x509.NewCertPool()
	ok := tlsCfg.RootCAs.AppendCertsFromPEM(caPemData)
	if !ok {
		log.Fatalf("failed to append ca to pool for tls: %s", err)
	}

	if cfg.TLSAuth {
		tlsCfg.ClientAuth = tls.RequireAndVerifyClientCert
	} else {
		tlsCfg.ClientAuth = tls.RequireAnyClientCert
	}

	serverPair, err := tls.LoadX509KeyPair(cfg.TLSCertFile, cfg.TLSKeyFile)
	if err != nil {
		log.Fatalf("failed to retreive key pair for tls: %s", err)
	}

	tlsCfg.Certificates = []tls.Certificate{serverPair}

	return tlsCfg
}

// RunGrpcServer .
func RunGrpcServer(ctx context.Context, log *logrus.Logger, cfg *config.Settings) error {
	factory, err := registry.NewInstanceFactory(ctx, cfg, log)
	if err != nil {
		return err
	}

	lis, err := net.Listen(cfg.Grpc.Bind.Scheme, cfg.Grpc.Bind.Host)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	opts := []grpc.ServerOption{}

	if cfg.Grpc.TLS {
		log.Info("Enable TLS")
		opts = append(opts, grpc.Creds(credentials.NewTLS(getTLSConfig(cfg.Grpc))))
	}

	if cfg.Grpc.TokenSecret != "" {
		log.Info("Enable Auth")
		opts = append(opts,
			grpc.UnaryInterceptor(makeAuthTokenInterceptor(defaultAuthHeaderName, func(token string) error {
				if token != cfg.Grpc.TokenSecret {
					return errInvalidToken
				}

				return nil
			})),
		)
	}

	grpcServer := grpc.NewServer(opts...)

	loadSvc := loadServer{
		rootCtx: ctx,
		factory: factory,
	}
	pbload.RegisterProductsLoadService(grpcServer, &pbload.ProductsLoadService{
		Fetch: loadSvc.Fetch,
	})

	querySvc := queryServer{
		rootCtx: ctx,
		factory: factory,
	}
	pbquery.RegisterQueryService(grpcServer, &pbquery.QueryService{
		GetList: querySvc.GetList,
	})

	go func() {
		select {
		case <-ctx.Done():
		}
		grpcServer.GracefulStop()
	}()

	log.Info("Serve GRPC server")
	grpcServer.Serve(lis)

	return nil
}

package grpcserver

import (
	"context"
	"strings"

	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"
)

var (
	errNoMetadata   = status.Errorf(codes.InvalidArgument, "No metadata")
	errInvalidToken = status.Errorf(codes.Unauthenticated, "invalid token")
)

func makeAuthTokenInterceptor(headerName string, tokenValidator func(string) error) grpc.UnaryServerInterceptor {
	return func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {
		md, ok := metadata.FromIncomingContext(ctx)
		if !ok {
			return nil, errNoMetadata
		}

		authHeader := md[headerName]
		if len(authHeader) == 0 {
			return nil, errInvalidToken
		}

		token := strings.TrimPrefix(authHeader[0], "Bearer ")

		err := tokenValidator(token)
		if err != nil {
			return nil, errInvalidToken
		}

		return handler(ctx, req)
	}
}

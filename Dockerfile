FROM golang:1.15 as builder

ARG BUILD_GIT_HASH=
ENV BUILD_GIT_HASH=$BUILD_GIT_HASH

WORKDIR /home/app

COPY cmd ./cmd
COPY internal ./internal
COPY pkg ./pkg
COPY go.mod \
     go.sum \
     config.yml \
     VERSION \
     Makefile ./
RUN make build


FROM alpine:3.12

WORKDIR /home/app
COPY config.yml config.yml
COPY entrypoint.sh /
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
RUN chmod 755 /entrypoint.sh && mkdir /lib64 && ln -s /lib/libc.musl-x86_64.so.1 /lib64/ld-linux-x86-64.so.2 && adduser -D app
COPY --from=builder /home/app/bin/prod-aggregator .

USER app

EXPOSE 10000
ENTRYPOINT ["/entrypoint.sh"]
CMD ["./prod-aggregator", "serve"]

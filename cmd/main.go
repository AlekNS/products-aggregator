package main

import (
	"fmt"
	"os"
	"strings"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/alekns/prod-aggregator/internal"
	"gitlab.com/alekns/prod-aggregator/internal/ext/cmds"
)

var mainCommand = &cobra.Command{
	Use:     "prod-aggregator",
	Long:    "prod-aggregator",
	Version: fmt.Sprintf("%s:%s", internal.AppVersion, internal.GitHash)}

func main() {
	viper.SetEnvPrefix("pa")
	viper.AutomaticEnv()
	replacer := strings.NewReplacer(".", "_")
	viper.SetEnvKeyReplacer(replacer)

	mainFlags := mainCommand.PersistentFlags()

	mainFlags.String("config-file", "", "Config file")
	mainFlags.String("logging-level", "", "Default console level")
	viper.BindPFlag("logging.console.level", mainFlags.Lookup("logging-level"))

	mainCommand.AddCommand(cmds.AggregateCommand())
	mainCommand.AddCommand(cmds.ServeCommand())
	mainCommand.AddCommand(cmds.BootstrapCommand())

	if mainCommand.Execute() != nil {
		os.Exit(1)
	}
}

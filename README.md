# Products Aggregator

## Run

1. `$ cd tests`
1. `$ docker-compose up -d aggregator service`
1. `$ docker-compose scale service=5`
1. `$ docker-compose up -d nginx`

Use any grpc client (without tls).


## TLS Mutual

Use crypto materials for client from `tests/tls/keys/client*.pem` and `tests/tls/ca.pem`.

Use some of override server name: `service`, `prodaggreagtor`, `prod-agg`, `prod-agg.com`.


## TODO

* Add tests.
* Add more verbosity of grpc errors.
* Improve query algorithm.
* Improve error messages.
* More verbosity logs.
* More configs.
* Add tracing.
* Add profiler endpoints.
* Try to improve alg for aggregation and uploading.
* Refactor large parts of code.
* Improve deployment.


[I'm here too](https://www.github.com/alekns)

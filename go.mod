module gitlab.com/alekns/prod-aggregator

go 1.15

require (
	github.com/gogo/protobuf v1.3.1
	github.com/golang/protobuf v1.4.2
	github.com/labstack/gommon v0.3.0
	github.com/sirupsen/logrus v1.7.0
	github.com/spf13/cobra v1.1.1
	github.com/spf13/viper v1.7.1
	go.mongodb.org/mongo-driver v1.4.4
	google.golang.org/grpc v1.34.0
)
